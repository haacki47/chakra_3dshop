const gulp = require('gulp'),
		jade = require('gulp-jade'),
		sass = require('gulp-sass'),
		connect = require('gulp-connect'),
		uglify = require('gulp-uglify'),
		watch = require('gulp-watch'),
		plumber = require('gulp-plumber'),
		prefix = require('gulp-autoprefixer'),
		concat = require('gulp-concat');

const dest = './public/';
const dist = './source/';

	gulp.task('connect', function () {
		connect.server({
			root: dest,
			port: 1337,
			livereload: true,
		});
	});

	gulp.task('jade:build', function  () {
		return gulp.src([dist + 'jade/**/*.jade', '!' + dist + 'jade/**/@*.jade'])
			.pipe(plumber())
			.pipe(jade({
				pretty: false
			}))
			.pipe(plumber.stop())
			.pipe(gulp.dest(dest))
			.pipe(connect.reload())
	});

	gulp.task('sass', function  () {
		return gulp.src(dist + 'sass/**/*.sass')
			.pipe(plumber())
			.pipe(sass())
			.pipe(prefix({
				browsers: ['last 6 versions'],
				cascade: false
			}))
			.pipe(plumber.stop())
			.pipe(gulp.dest(dest + 'css/'))
			.pipe(connect.reload())
	});

	gulp.task('js', function  () {
		return gulp.src(dist + 'js/**/*.js')
			.pipe(plumber())
			.pipe(uglify())
			.pipe(concat('3dshop.min.js'))
			.pipe(plumber.stop())
			.pipe(gulp.dest(dest + 'js/'))
			.pipe(connect.reload())
	});

	gulp.task('images:copy', function  () {
		return gulp.src(dist + 'images/**/*')
			.pipe(gulp.dest(dest + 'images/'))
			.pipe(connect.reload())
	});

	gulp.task('fonts:copy', function  () {
		return gulp.src(dist + 'fonts/**/*')
			.pipe(gulp.dest(dest + 'fonts/'))
			.pipe(connect.reload())
	});

	gulp.task('vendor:copy', function  () {
		return gulp.src(dist + 'vendor/**/*')
			.pipe(gulp.dest(dest + 'vendor/'))
			.pipe(connect.reload())
	});

	gulp.task('stream', function () {
		watch([
					dist + 'jade/**/*.jade', 
					dist + 'jade/**/@*.jade',
					dist + 'jade/**',
					dist + 'jade/**/*'], function (event, cb) {
			gulp.start('jade:build');
		});

		watch(dist + 'sass/**/*.sass', function  (event, cb) {
			gulp.start('sass');
		});

		watch(dist + 'js/**/*.js', function  (event, cb) {
			gulp.start('js');
		});

		watch(dist + 'images/**/*', function  (event, cb) {
			gulp.start('images:copy');
		});

		watch(dist + 'fonts/**/*',function (event, cb) {
			gulp.start('fonts:copy');
		});

		watch(dist + 'vendor:copy', function  (event, cb) {
			gulp.start('vendor:copy');
		})

	});

	gulp.task('default', ['connect', 'jade:build', 'sass', 'js', 'images:copy', 'fonts:copy', 'vendor:copy', 'stream']);

	gulp.task('production', ['jade:build', 'sass', 'js', 'images:copy', 'vendor:copy', 'fonts:copy'])