jQuery(document).ready(function  () {
	var vcxcrsl = $('#vcx21-crslid');

	if(vcxcrsl) {
		vcxcrsl.slick({
			dots: false,
			infinite: true,
			speed: 300,
			draggable: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			focusOnSelect: false,
			prevArrow: '.vcx_slick-custom-left',
			nextArrow: '.vcx_slick-custom-right',
			responsive: [
				{
				breakpoint: 1200,
				settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					}
				}
			],
		})
	}

});