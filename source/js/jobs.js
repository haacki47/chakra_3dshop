jQuery(document).ready(function  () {
	var sldx44 = $('#sldx44-crslid');

	if(sldx44){
		sldx44.slick({
			dots: false,
			infinite: true,
			speed: 300,
			draggable: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			prevArrow: '.sldx_slick-custom-left',
			nextArrow: '.sldx_slick-custom-right',
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToScroll: 1,
						slidesToShow: 3,
					}
				}
			]
		})
	}

});