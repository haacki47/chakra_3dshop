jQuery(document).ready(function () {

	

	$('#oborud').masonry({
		gutter: 32,
		itemSelector: '.focusing-at',
	});

	$('#uslugy').masonry({
		gutter: 32,
		itemSelector: '.focusing-at',
	});

	$('#reshen').masonry({
		gutter: 32,
		itemSelector: '.focusing-at',
	});

	$('#mater').masonry({
		gutter: 32,
		itemSelector: '.focusing-at',
	});

	(function () {
		var crxAnch = $('.tbx-zxs47 .tabs>li>a'),
				crxCont = $('.tbx-zxs47 .caty');
		var curtbx = $('.tbx-zxs47');
		crxCont.css('display','none');
		crxCont.first().css('display', 'block')

		crxAnch.click(function (e) {
			e.preventDefault();

			var crxCur = $(this).attr('href');

			if($(this).parent().hasClass('active')) return;

			else {
				crxAnch.parent().removeClass('active');
				$(this).parent().addClass('active');
				crxCont.fadeOut();
				curtbx.find(crxCur).fadeIn()
			}
		});

	})();

});