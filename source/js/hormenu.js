jQuery(document).ready(function  () {
	var drp, listEvent, drplevel, listParent;

	var drpflx = {
		display: '-webkit-flex',
		display: '-ms-flex',
		display: 'flex',
		borderTop: '2px solid rgb(255, 221, 0)',
	};

	var drpParent = {
		backgroundColor: 'rgb(255, 221, 0)',
	};
	/*
		@FINDING THE MENU AND ADD A EVENT
		@WITH EVENT HOVER MENU, CHILD UL WILL DROPDOWN
	*/

	drp = $('#drp-xvc21');
	listEvent = drp.find('.parent-dropdown');

	/*
		@ADD EVENT FOR HOVER
		@IF USER MOUSE OUT, DO: DISPLAY: NONE; FOR MENU
	*/

	listEvent.on('mouseover', function  () {
		drplevel = $(this).find('ul.dropdown-menu');
		drplevel.css(drpflx);
		$(this).css(drpParent);

		$(this).on('mouseleave', function  () {
			drplevel.fadeOut();
			$(this).removeAttr('style');
		});
	});

});