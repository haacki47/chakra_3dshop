jQuery(document).ready(function  () {

	var proud, sldxprev, sldximg;

	console.log($('.proud-sldx-img'));

	$('.proud-sldx-img').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.proud-sldx-prev'
	});

	$('.proud-sldx-prev').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.proud-sldx-img',
		dots: false,
		arrows: false,
		centerMode: false,
		focusOnSelect: true,
	});

});